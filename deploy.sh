#!/bin/bash

set -ex

echo "$DEPLOYKEY" > /tmp/key && chmod 600 /tmp/key

export GIT_SSH_COMMAND="ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -i/tmp/key"
git clone git@codeberg.org:ForgeFed/pages.git /tmp/pages
rm -fr /tmp/pages/*
cp -a html/* /tmp/pages
cd /tmp/pages
cat > .domains <<EOF
forgefed.org
www.forgefed.org
EOF
git add .
if git diff --staged --exit-code >& /dev/null ; then
    echo No changes to push
else
    git config user.email contact@forgefed.io
    git config user.name 'Deploy Bot'
    git commit -m 'Update from deploy.sh'
    git push -u origin main
fi
